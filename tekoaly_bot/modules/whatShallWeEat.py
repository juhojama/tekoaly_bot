import calendar
import datetime
import re

import requests

from ..utils import emoji_cheatcheet, finnish_weekdays
from ..conf.conf import logger

# kastari_url_fi = "https://www.uniresta.fi/export/rss-kastari.json"
# kastari_url_en = "https://www.uniresta.fi/en/export/rss-kastari.json"


class UnirestaMenu:
    def __init__(self):
        self.corporation = "Uniresta"
        self.weekdays = {0: "ma", 1: "ti", 2: "ke", 3: "to", 4: "pe", 5: "la", 6: "ma"}

    def get_corporation(self):
        return self.get_corporation

    def fetch_food(self, restaurant, language):
        if language == "fi":
            lang = ""
        else:
            lang = language + "/"

        # Generate URI dynamically based on language and restaurant

        URI = "https://www.uniresta.fi/{}export/rss-{}.json".format(lang, restaurant)
        response = requests.get(URI)

        # Return False if no match for restaurant name or site is down
        if response.status_code != 200:
            return False
        food = response.json()
        return self.parse_json_uniresta(food)

    def parse_json_uniresta(self, json):

        current_language = "fi" if json.get("page").get("language") == "FI" else "en"

        # if json.get("page").get("language") == "EN":
        #    subsection_name = "463"
        # else:
        #    subsection_name = "421"

        # Get current daytime and shorten it for future use
        now = datetime.datetime.now()
        final_message = ""
        today_message_en = "Today is the {}, {}. day of {}!\nWhat shall we eat today? {}\n".format(
            calendar.day_name[now.weekday()],
            now.day,
            now.strftime("%B"),
            emoji_cheatcheet.get("thinking"),
        )
        today_message_fi = "Tänään on {} ja viikko on {}!\nMitähän tänään syötäisiin? {}\n".format(
            finnish_weekdays[now.weekday()],
            now.isocalendar()[1],
            emoji_cheatcheet.get("thinking"),
        )

        final_message += (
            today_message_fi if current_language == "fi" else today_message_en
        )

        # Get main data object
        sections = json.get("sections")
        sections_sub = next(iter(sections.values()))
        current_week = ""
        for index, week in enumerate(sections_sub):

            # Set first index as default week, Uniresta is returning next week menu, if looking for it in Sunday
            if index == 0:
                current_week = week

            # Change menu for current week
            if now.isocalendar()[1] == int(re.search(r"\d+", week.get("name")).group()):
                relased_menu_en = "The most recent menu has been released on {}!\n".format(
                    week.get("julkaisupaiva")
                )
                relased_menu_fi = "Uusin ruokalista on julkaistu {}!\n".format(
                    week.get("julkaisupaiva")
                )
                final_message += (
                    relased_menu_fi if current_language == "fi" else relased_menu_en
                )
                current_week = week

        # Currently showing menu of current day OR next available day

        # TODO make option to select day and week

        # Note sunday, when places are closed, set for monday
        current_week_day = (
            finnish_weekdays[now.weekday()].capitalize()
            if now.weekday() != 6
            else finnish_weekdays[0].capitalize()
        )

        # message_width = 5+6+2+len(current_week.get("categories")[0])+ len(current_week_day)
        # final_message += "{s:{p}^{w}}\n".format(s="", p = "-", w= message_width)

        final_message += "\n*## {} ## {} #*\n".format(
            current_week.get("categories")[0], current_week_day
        )

        # final_message += "{s:{p}^{w}}\n".format(s="", p = "-", w= message_width)

        for title in current_week.get(
            "{}-erikoisuudet-otsikot".format(self.weekdays.get(now.weekday()))
        ):
            final_message += title + "\n"

        final_message += "\n*#### Menu ####*\n"

        for i in range(1, 7):
            food = current_week.get(
                "{}-ruokalaji-{}".format(self.weekdays.get(now.weekday()), i)
            )
            if food is not None:
                final_message += food + "\n"

        # Removed specials
        # TODO make it optional

        # inal_message += "\n---------\nSpecial\n---------\n"
        # for special in current_week.get("{}-erikoisuudet".format(self.weekdays.get(now.weekday()))):
        #    final_message += special+"\n"

        return final_message

    #def daily_meal(self, bot, update, args):
    def daily_meal(self, update, context):
        logger.debug("Received food menu request.")
        restaurant = "kastari" if len(context.args) < 1 else context.args[0]
        language = "fi" if len(context.args) < 2 else context.args[1]
        meal = self.fetch_food(restaurant=restaurant, language=language)
        if meal:
            context.bot.send_message(
                chat_id=update.message.chat_id, text=meal, parse_mode="Markdown"
            )
            return

        not_found_en = (
            "No restaurant was specified or specified restaurant was not found!  \n  \nFor example, "
            'get menu of Kastari in English use "/ruoka kastari en" command  \n  \nCheck available '
            "Uniresta restaurants at https://www.uniresta.fi/lounasravintolat/kaikki-ravintolat.html"
        )
        not_found_fi = (
            "Ei olemassa olevaa ravintolaa määritelty tai määritelty ravintola ei löytynyt.  \n  "
            '\nSaadaksesi esimerkiksi Kastarin ruokalistan suomeksi, aja "/ruoka kastari fi" komentoa  '
            "\n  \nTarkista mahdolliset Unirestan ravintolat osoitteessa "
            "https://www.uniresta.fi/lounasravintolat/kaikki-ravintolat.html"
        )

        not_found = not_found_fi if language == "fi" else not_found_en
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=not_found,
            parse_mode="Markdown",
            disable_web_page_preview=True,
        )
