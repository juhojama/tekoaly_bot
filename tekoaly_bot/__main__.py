import telegram
from telegram.ext import CommandHandler, CallbackQueryHandler, PrefixHandler
from telegram.ext import Updater
from dataclasses import dataclass
from .conf.keys import BOT_TOKEN
from .modules.weather import GetWeather
from .modules.chuckNorris import ChuckNorris
from .modules.goodMorning import GoodMorning
from .modules.whatShallWeEat import UnirestaMenu
from .modules.hackernews import GetHackyNews

from .conf.conf import logger


@dataclass
class FeatureDataStorage:
    good_morning: GoodMorning
    uniresta: UnirestaMenu
    weather: GetWeather
    norrisjoke: ChuckNorris
    hackernews: GetHackyNews


bot = telegram.Bot(token=BOT_TOKEN)  # set bot
updater = Updater(BOT_TOKEN, use_context=True)
dispatcher = updater.dispatcher
job_queue = updater.job_queue
datastorage = FeatureDataStorage(
    GoodMorning(job_queue),
    UnirestaMenu(),
    GetWeather(bot),
    ChuckNorris(bot),
    GetHackyNews(bot),
)

"""
Log Errors caused by Updates.
"""


def error(bot, update, error):
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():

    huomenta_handler = CommandHandler(
        "huomenta", datastorage.good_morning.handle_command
    )
    dispatcher.add_handler(huomenta_handler)

    food_handler = CommandHandler("ruoka_aika", datastorage.uniresta.daily_meal)
    dispatcher.add_handler(food_handler)

    weather_handler = CommandHandler("saa", datastorage.weather.handle_command)
    dispatcher.add_handler(weather_handler)

    joke_handler = CommandHandler("norris", datastorage.norrisjoke.handle_command)
    dispatcher.add_handler(joke_handler)

    hacker_handler = CommandHandler("hackernews", datastorage.hackernews.handle_command)
    dispatcher.add_handler(hacker_handler)
    dispatcher.add_handler(CallbackQueryHandler(newsBrowserQueryForwarder))
    updater.start_polling()
    updater.idle()


def newsBrowserQueryForwarder(update, context):
    """
    TODO make global callbackquery handler.  Now it is working
    only for one specific case in hackernews.
    """
    datastorage.hackernews.newsBrowserQueryHandler(update, context)


if __name__ == "__main__":
    main()
