import logging
import sys

# Default location coords are in the center of Oulu

DEFAULT_LOCATION_COORDS = {"lat": 65.0119, "lng": 25.4717}
DEFAULT_COUNTRY = "Finland"
DEFAULT_CITY = "Oulu"


"""
### Logging configuration ###
"""

LOGGING_LEVEL = logging.DEBUG
logger = logging.getLogger("telegram.bot")
logger.setLevel(LOGGING_LEVEL)

# Formatter of log
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(LOGGING_LEVEL)
handler.setFormatter(formatter)
logger.addHandler(handler)
