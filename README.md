# tekoaly_bot
A simple bot for Telegram.

## How to run
1. Install python3
2. Create virtual environment for project packages: 
```python -m venv telegram_bot_venv```
3. Activate environment: ```source telegram_bot_venv/bin/activate``` 
4. Install required modules from requirements.txt: ```pip install -r requirements.txt```
5. Create file TOKEN.txt and add bot token to the 1st line
6. Run  as module `python3 -m tekoaly_bot`

## Docker

For using Docker:

```
# Building the image
docker build -t tekoaly_bot .

# Starting up a container, outside port is 8080
docker run -p 8080:8080 tekoaly_bot

# Or running in background as daemon using default ports

docker run -d -t tekoaly_bot

```
