FROM python:3-alpine

RUN mkdir -p /usr/src/bot
WORKDIR /usr/src/bot

COPY requirements.txt /usr/src/bot/

RUN apk update && apk upgrade
RUN apk add gcc libffi-dev musl-dev openssl-dev
RUN pip3 install --no-cache-dir -r requirements.txt

COPY . /usr/src/bot

EXPOSE 8080

ENTRYPOINT ["python3"]

CMD ["-m", "tekoaly_bot"]
